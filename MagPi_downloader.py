import argparse
import urllib.request
import time

URL = "https://www.raspberrypi.org/magpi-issues/"
ISSUE = "MagPi"
DESCRIPTION="Download the MagPi issues without doing anything"

hdr = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11',
       'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
       'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
       'Accept-Encoding': 'none',
       'Accept-Language': 'en-US,en;q=0.8',
       'Connection': 'keep-alive'}


def download_issue(issue, destination_folder):
    """
        Descarga el número de la revista especificado.
        Parametros:
            issue: El número a descargar
            destination_dolder: Ruta a en la que lo guardará.
    """
    file_name = "".join([ISSUE, str(issue), ".pdf"])
    link = "".join([URL,file_name])
    request = urllib.request.Request(link, headers=hdr)
    quest = urllib.request.urlopen(request)
    if (quest.getcode()== 404):
        raise URLError("No se ha encontrado el issue solicitado")
    if (quest.getcode() != 200):
        raise URLError("Error al conectar al servidor")
    with open("".join(file_name),"wb") as fhand:
        fhand.write(quest.read())
    return 0

def Download_special(issue, destination_folder="."):
    pass

def argsparsed(args):
    """
        Determina si descargar 1 o más número de la revista
    """
    destination = args.output if args.output != None else "." 
    if (args.last_issue == None):
        time_counter = time.time()
        download_issue(args.first_issue, args.output)
        print("Downloaded issue", args.first_issue, "in", round(time.time()-time_counter),"s")
    else:
        time_counter = time.time()
        for issue in range(args.first_issue, args.last_issue+1):
            download_issue(issue, args.output)
        print("Downloaded", args.last_issue - args.first_issue, "issues in", round(time.time()-time_counter,"s"))   
    

def main():
    parser = argparse.ArgumentParser(description=DESCRIPTION)
    parser.add_argument("-output", help="Destination folder", dest="output", type=str, required=False)
    parser.add_argument("-issue", help="Which issue to download. In case you also specify -until, it will download every issue between -issue and -until", type=int, dest="first_issue" , required=True)
    parser.add_argument("-until", help="Download all issues between -issue and -until", dest="last_issue", type=int, required=False)
    parser.set_defaults(func=argsparsed)
    args=parser.parse_args()
    argsparsed(args)

if __name__ == "__main__":
    main()
